#ifndef MY_GL_H
#define MY_GL_H

#include "geometry.h"
#include "global_values.h"
#include "model.h"
#include "QPainter"
#include "tgaimage.h"

class IShader
{
public:
    Model *model;
    virtual ~IShader();
    virtual Vec4f vertex(int iface, int nthvert) = 0;
    virtual bool fragment(Vec3f bar, TGAColor &color) = 0;
    void set_model(Model *m);
};

class Shader : public IShader
{
public:
    mat<2,3,float> varying_uv;  // triangle uv coordinates, written by the vertex shader, read by the fragment shader
    mat<4,3,float> varying_tri; // triangle coordinates (clip coordinates), written by VS, read by FS
    mat<3,3,float> varying_nrm; // normal per vertex to be interpolated by FS
    mat<3,3,float> ndc_tri;     // triangle in normalized device coordinates

    virtual Vec4f vertex(int iface, int nthvert);
    virtual bool fragment(Vec3f bar, TGAColor &color);
};

void viewport(Matrix &V, int x, int y, int w, int h);
void projection(Matrix &P, float coeff = 0.f);
void lookat(Matrix &MV, Vec3f eye, Vec3f center, Vec3f up);

void line(int x0, int y0, int x1, int y1, TGAImage &image, TGAColor color);
void line(int x0, int y0, int x1, int y1, QPainter *image, QColor color);
void triangle(Vec3i t0, Vec3i t1, Vec3i t2, TGAImage &image, TGAColor color, int *zbuffer);
void triangle(Vec3i t0, Vec3i t1, Vec3i t2, QPainter *image, QColor color, int *zbuffer);
void triangle(mat<4,3,float> &clipc, IShader &shader, TGAImage &image, float *zbuffer);
void triangle(mat<4,3,float> &clipc, IShader &shader, QPainter *image, float *zbuffer);

#endif // MY_GL_H
