#ifndef GLOBAL_VALUES_H
#define GLOBAL_VALUES_H

#include "geometry.h"
#include "model.h"

extern Vec3f light_dir2;
extern Vec3f ld;
extern Vec3f eye;
extern Vec3f center_view;
extern Vec3f up;

extern Matrix ModelView;
extern Matrix Projection;
extern Matrix Viewport;

#endif // GLOBAL_VALUES_H
