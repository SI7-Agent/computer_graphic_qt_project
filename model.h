#ifndef MODEL_H
#define MODEL_H

#define THREADS_NUMBER 8

#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "geometry.h"
#include "tgaimage.h"

class Model
{
private:
    std::vector<Vec3f> verts_;
    std::vector<std::vector<Vec3i> > faces_;
    std::vector<Vec3f> norms_;
    std::vector<Vec2f> uv_;
    TGAImage diffusemap_;
    TGAImage normalmap_;
    TGAImage specularmap_;

    void load_texture(std::string filename, const char *suffix, TGAImage &img);

public:
    bool flag_textures;
    float ddx;
    float ddy;

    Model(const char *filename);
    ~Model();
    int nverts();
    int nfaces();
    void set_vert(int i, Vec3f value);
    Vec3f normal(int iface, int nthvert);
    Vec3f normal(Vec2f uv);
    Vec3f vert(int i);
    Vec3f vert(int iface, int nthvert);
    Vec2f uv(int iface, int nthvert);
    TGAColor diffuse(Vec2f uv);
    float specular(Vec2f uv);
    std::vector<int> face(int idx);

    void rotate_left(double angle);
    void rotate_right(double angle);
    void rotate_up(double angle);
    void rotate_down(double angle);

    void move_left(float dx);
    void move_right(float dx);
    void move_up(float dy);
    void move_down(float dy);

    TGAImage diffuse_m();
    TGAImage normal_m();
    TGAImage specular_m();
};
#endif //__MODEL_H__

