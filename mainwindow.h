#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <limits>
#include <math.h>

#include <QColor>
#include <QColorDialog>
#include <QDebug>
#include <QFileDialog>
#include <QGraphicsView>
#include <QImage>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QMouseEvent>
#include <QShortcut>

#include "geometry.h"
#include "global_values.h"
#include "model.h"
#include "my_gl.h"
#include "tgaimage.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    bool flag_t;
    bool flag_c;
    explicit MainWindow(QWidget *parent = nullptr);
    void find_scale();
    void refresh();
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QColor tonning_color;
    QPainter *paint;
    QImage img;
    QPixmap *px, *px_toned;
    std::vector<Model*> model;
    std::vector<QString> model_name;
    int x, y;
    float kx, ky;
    float tkx, tky;
    bool pressed_mouse;
    Vec3f light_dir;

    QShortcut *key_ctrl_plus;
    QShortcut *key_ctrl_minus;
    QShortcut *key_shift_w;
    QShortcut *key_shift_s;
    QShortcut *key_shift_a;
    QShortcut *key_shift_d;


    void draw_web();
    void draw_tonned();
    void draw_textures();
    bool check_textures();
    void set_enables();

private slots:
    void on_load_model_clicked();
    void on_clear_scene_clicked();
    void on_render_clicked();
    void on_save_clicked();
    void on_color_picker_clicked();
    void on_intensivity_valueChanged(int value);
    void on_x_light_valueChanged(int value);
    void on_y_light_valueChanged(int value);

    void ctrl_plus();
    void ctrl_minus();
    void shift_w();
    void shift_s();
    void shift_a();
    void shift_d();
    void on_render_carcas_clicked();
};

#endif // MAINWINDOW_H
