#include "errorwindow.h"
#include "ui_errorwindow.h"
#include "sha256.h"

#include <QMainWindow>

ErrorWindow::ErrorWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ErrorWindow)
{
    ui->setupUi((QMainWindow*)this);
}

ErrorWindow::~ErrorWindow()
{
    delete ui;
}

void ErrorWindow::on_exit_button_clicked()
{
    QApplication::exit(0);
}

void ErrorWindow::on_install_button_clicked()
{
    system("render_installer.exe");
    QApplication::exit(0);
}
