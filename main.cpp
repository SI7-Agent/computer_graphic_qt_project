#include "mainwindow.h"
#include "errorwindow.h"
#include "sha256.h"

#include <QApplication>
#include "QDebug"

int start_application(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

int show_error(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ErrorWindow e;
    e.show();

    return a.exec();
}

int main(int argc, char *argv[])
{
    bool is_licensed = check_UUID();

    if (is_licensed)
    {
        return start_application(argc, argv);
    }
    else
    {
        return show_error(argc, argv);
    }
}
