#ifndef ERRORWINDOW_H
#define ERRORWINDOW_H

#include <QWidget>

namespace Ui {
class ErrorWindow;
}

class ErrorWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ErrorWindow(QWidget *parent = nullptr);
    ~ErrorWindow();

private slots:
    void on_exit_button_clicked();
    void on_install_button_clicked();

private:
    Ui::ErrorWindow *ui;
};

#endif // ERRORWINDOW_H
