#-------------------------------------------------
#
# Project created by QtCreator 2019-08-28T16:29:36
#
#-------------------------------------------------

QT       += core gui
CONFIG   += с++17

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = render_interactive
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    errorwindow.cpp \
    global_values.cpp \
        main.cpp \
        mainwindow.cpp \
    geometry.cpp \
    model.cpp \
    sha256.cpp \
    tgaimage.cpp \
    my_gl.cpp \

HEADERS += \
    errorwindow.h \
    global_values.h \
        mainwindow.h \
    geometry.h \
    model.h \
    sha256.h \
    tgaimage.h \
    my_gl.h \

FORMS += \
        errorwindow.ui \
        mainwindow.ui
