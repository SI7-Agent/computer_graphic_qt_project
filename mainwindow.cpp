#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::set_enables()
{
    if (flag_c)
    {
        ui->x_light->setEnabled(false);
        ui->y_light->setEnabled(false);
        ui->intensivity->setEnabled(false);
        ui->color_picker->setEnabled(true);
    }
    else
    {
        if (ui->check_textures->isChecked())
        {
            ui->x_light->setEnabled(true);
            ui->y_light->setEnabled(true);
            ui->intensivity->setEnabled(false);
            ui->color_picker->setEnabled(false);
        }
        else
        {
            ui->x_light->setEnabled(false);
            ui->y_light->setEnabled(false);
            ui->intensivity->setEnabled(true);
            ui->color_picker->setEnabled(true);
        }
    }
}

bool MainWindow::check_textures()
{
    bool res = true;
    for (unsigned int i = 0; i < model.size(); ++i)
    {
        if (!model[i]->flag_textures)
        {
            res = !res;
            break;
        }
    }

    return res;
}

void MainWindow::draw_textures()
{
    float *zbuffer = new float[800*800];
    for (int i = 800*800; i--; zbuffer[i] = -std::numeric_limits<float>::max());
    int weight = this->ui->draw_label->width();
    int height = this->ui->draw_label->height();

    lookat(ModelView, eye, center_view, up);
    viewport(Viewport, weight/(8*tkx), height/(8*tky), weight*3/(4*tkx), height*3/(4*tky));
    projection(Projection, -1.f/(eye-center_view).norm());
    light_dir2 = proj<3>((Projection*ModelView*embed<4>(light_dir2, 0.f))).normalize();

    Shader shader;
    for (unsigned int n = 0; n < this->model.size(); ++n)
    {
        shader.set_model(model[n]);
        for (int i = 0; i < model[n]->nfaces(); i++)
        {
            for (int j = 0; j < 3; j++)
            {
                shader.vertex(i, j);
            }

            triangle(shader.varying_tri, shader, this->paint, zbuffer);
        }
    }

    ui->draw_label->setPixmap(*px);
    delete[] zbuffer;
}


void MainWindow::draw_web()
{
    int h = ui->draw_label->height(), w = ui->draw_label->width();
    int _2w = w/2, _2h = h/2;

    for (unsigned int n=0; n<model.size(); n++)
    {
        for (int i=0; i<model[n]->nfaces(); i++)
        {
            std::vector<int> face = model[n]->face(i);
            for (int j=0; j<3; j++)
            {
                Vec3f v0 = model[n]->vert(face[j]);
                Vec3f v1 = model[n]->vert(face[(j+1)%3]);

                int x0 = (v0.x)*kx + _2w;
                int y0 = h - (v0.y*ky) - _2h;
                int x1 = (v1.x)*kx + _2w;
                int y1 = h - (v1.y*ky) - _2h;
                line(x0, y0, x1, y1, paint, tonning_color);
            }
        }
    }

    ui->draw_label->setPixmap(*px);
}

void MainWindow::draw_tonned()
{
    int r,g,b;
    this->tonning_color.getRgb(&r, &g, &b);
    int h = ui->draw_label->height();

    int *zbuffer = new int[800*800];
    for (int i = 0; i < 800*800; i++)
    {
        zbuffer[i] = std::numeric_limits<int>::min();
    }

    float g_intens = (float)ui->intensivity->value()/100;

    for (unsigned int i = 0; i < model.size(); i++)
    {
        for (int k = 0; k < model[i]->nfaces(); k++)
        {
            std::vector<int> face = model[i]->face(k);
            Vec3i screen_coords[3];
            Vec3f world_coords[3];
            for (int j = 0; j < 3; j++)
            {
                Vec3f v = model[i]->vert(face[j]);
                screen_coords[j] = Vec3i((v.x+1.)*kx, h - (v.y+1.)*ky, (v.z+1.)*128/2.);
                world_coords[j]  = v;
            }
            Vec3f n = (world_coords[2]-world_coords[0])^(world_coords[1]-world_coords[0]);
            n.normalize();
            float intensity = n*light_dir;
            if (intensity < 0)
            {
                intensity *= -1;
            }

            triangle(screen_coords[0], screen_coords[1], screen_coords[2], this->paint, QColor(r*intensity*g_intens, g*intensity*g_intens, b*intensity*g_intens), zbuffer);
        }
    }

    ui->draw_label->setPixmap(*px);
    delete[] zbuffer;
}

void MainWindow::find_scale()
{
    int w = ui->draw_label->width(), h = ui->draw_label->height();

    float max_x = model[0]->vert(0)[0]*w;
    float min_x = model[0]->vert(0)[0]*w;
    float max_y = model[0]->vert(0)[1]*h;
    float min_y = model[0]->vert(0)[1]*h;

    for (unsigned int n=0; n<model.size(); n++)
    {
        for (int i = 0; i < model[n]->nverts(); i++)
        {
            max_x = std::max(max_x, model[n]->vert(i)[0]*w);
            min_x = std::min(min_x, model[n]->vert(i)[0]*w);
            max_y = std::max(max_y, model[n]->vert(i)[1]*h);
            min_y = std::min(min_y, model[n]->vert(i)[1]*h);
        }
    }

    kx = w*(w-10) / (max_x-min_x);
    ky = h*(h-65) / (max_y-min_y);
    kx>ky ? kx=ky : ky=kx;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    flag_t(true), flag_c(true),
    ui(new Ui::MainWindow),
    kx(0), ky(0),
    tkx(1), tky(1),
    pressed_mouse(false)
{
    ui->setupUi(this);
    tonning_color = QColor(Qt::white);
    px = new QPixmap(ui->draw_label->width(), ui->draw_label->height());
    px->fill();
    px_toned = new QPixmap(ui->color_toned->width(), ui->color_toned->height());
    px_toned->fill(tonning_color);
    paint = new QPainter(px);
    paint->setPen(tonning_color);
    ui->draw_label->setPixmap(*px);
    ui->color_toned->setPixmap(*px_toned);
    light_dir = Vec3f(0,0,-1);
    this->set_enables();

    key_ctrl_plus = new QShortcut(this);
    key_ctrl_plus->setKey(Qt::CTRL + Qt::Key_Equal);
    connect(key_ctrl_plus, SIGNAL(activated()), this, SLOT(ctrl_plus()));

    key_ctrl_minus = new QShortcut(this);
    key_ctrl_minus->setKey(Qt::CTRL + Qt::Key_Minus);
    connect(key_ctrl_minus, SIGNAL(activated()), this, SLOT(ctrl_minus()));

    key_shift_s = new QShortcut(this);
    key_shift_s->setKey(Qt::SHIFT + Qt::Key_S);
    connect(key_shift_s, SIGNAL(activated()), this, SLOT(shift_s()));

    key_shift_w = new QShortcut(this);
    key_shift_w->setKey(Qt::SHIFT + Qt::Key_W);
    connect(key_shift_w, SIGNAL(activated()), this, SLOT(shift_w()));

    key_shift_a = new QShortcut(this);
    key_shift_a->setKey(Qt::SHIFT + Qt::Key_A);
    connect(key_shift_a, SIGNAL(activated()), this, SLOT(shift_a()));

    key_shift_d = new QShortcut(this);
    key_shift_d->setKey(Qt::SHIFT + Qt::Key_D);
    connect(key_shift_d, SIGNAL(activated()), this, SLOT(shift_d()));
}

void MainWindow::on_load_model_clicked()
{
    QFileDialog dialog(this);

    dialog.setDirectory(QDir::currentPath() + QString("/obj"));
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter(tr("Models (*.obj)"));
    dialog.setViewMode(QFileDialog::Detail);
    QStringList fileNames;
    if (dialog.exec())
    {
        fileNames = dialog.selectedFiles();

        model_name.push_back(QString(fileNames.join("")));

        Model* loaded = new Model(model_name[model_name.size()-1].toLocal8Bit());
        model.push_back(loaded);
        if (flag_t)
            flag_t = loaded->flag_textures;
        this->find_scale();
        QMessageBox::information(this, "Внимание","Модель успешно загружена");
    }
}

void MainWindow::on_render_carcas_clicked()
{
    flag_c = true;
    if (!model_name.empty())
    {
        this->set_enables();
        this->refresh();

        this->draw_web();
    }
    else
    {
        QMessageBox::information(this, "Внимание","Нет загруженных моделей для каркасного рендера");
    }
}

void MainWindow::on_render_clicked()
{
    flag_c = false;
    if (!model_name.empty())
    {
        this->set_enables();
        this->refresh();

        if (ui->check_textures->isChecked())
        {
            if (this->check_textures())
            {
                this->draw_textures();
            }
            else
            {
                QMessageBox::information(this, "Внимание","Не могу найти текстуры\nБудет выполнено тонирование загруженной модели");
                ui->check_textures->setChecked(false);
                set_enables();
                this->draw_tonned();
            }
        }
        else
        {
            this->draw_tonned();
        }
    }
    else
    {
        QMessageBox::information(this, "Внимание","Нет загруженных моделей для рендера");
    }
}

void MainWindow::on_save_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Document"),
                                                    QDir::currentPath(), tr("Pictures (*.png)"));
    px->save(filename);
}

void MainWindow::on_clear_scene_clicked()
{
    if (!model.empty())
    {
        model.clear();
        model_name.clear();
        this->refresh();
        QMessageBox::information(this, "Внимание","Сцена очищена\nМодели выгружены из памяти");
    }
    else
    {
        QMessageBox::information(this, "Внимание","Сцена пуста");
    }
}

void MainWindow::on_color_picker_clicked()
{
    tonning_color = QColorDialog::getColor();
    px_toned->fill(tonning_color);
    ui->color_toned->setPixmap(*px_toned);
}

void MainWindow::refresh()
{
    px->fill();
    ui->draw_label->setPixmap(*px);
}

void MainWindow::mousePressEvent(QMouseEvent *e)
{
    pressed_mouse = true;
    x = e->pos().x();
    y = e->pos().y();
}

void MainWindow::mouseMoveEvent(QMouseEvent *e)
{
    if (!model_name.empty())
    {
        double angle = M_PI/18;

        if (pressed_mouse)
        {
            if (x > e->pos().x())
            {
                for (unsigned int i = 0; i < model.size(); i++)
                    model[i]->rotate_right(angle);
            }

            if (x < e->pos().x())
            {
                for (unsigned int i = 0; i < model.size(); i++)
                    model[i]->rotate_left(angle);
            }

            if (y > e->pos().y())
            {
                for (unsigned int i = 0; i < model.size(); i++)
                    model[i]->rotate_up(angle);
            }

            if (y < e->pos().y())
            {
                for (unsigned int i = 0; i < model.size(); i++)
                    model[i]->rotate_down(angle);
            }

            this->refresh();

            if (flag_c)
            {
                this->draw_web();
            }
            else
            {
                if (ui->check_textures->isChecked())
                    this->draw_textures();
                else
                    this->draw_tonned();
            }

            x = e->pos().x();
            y = e->pos().y();
        }
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
    pressed_mouse = false;
}

MainWindow::~MainWindow()
{
    paint->end();
    delete ui;
    delete px;
    delete px_toned;
    delete paint;
    for (unsigned int n = 0; n < model.size(); n++)
        delete model[n];
}

void MainWindow::on_intensivity_valueChanged(int value)
{
    if (!ui->check_textures->isChecked())
    {
        this->refresh();
        this->draw_tonned();
    }
}

void MainWindow::on_x_light_valueChanged(int value)
{
    if (ui->check_textures->isChecked() and flag_t)
    {
        this->refresh();
        ld = Vec3f(value, ld[1], ld[2]);
        light_dir2 = ld;
        this->draw_textures();
    }
}

void MainWindow::on_y_light_valueChanged(int value)
{
    if (ui->check_textures->isChecked() and flag_t)
    {
        this->refresh();
        ld = Vec3f(ld[0], value, ld[2]);
        light_dir2 = ld;
        this->draw_textures();
    }
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (!model_name.empty())
    {
        float dx = 20/kx, dy = 20/ky;

        if (e->nativeVirtualKey() == Qt::Key_W)
        {
            for (unsigned int i = 0; i < model.size(); i++)
            {
                model[i]->move_up(dy);
                model[i]->ddy += dy;
            }
        }

        if (e->nativeVirtualKey() == Qt::Key_A)
        {
            for (unsigned int i = 0; i < model.size(); i++)
            {
                model[i]->move_left(dx);
                model[i]->ddx -= dx;
            }
        }

        if (e->nativeVirtualKey() == Qt::Key_S)
        {
            for (unsigned int i = 0; i < model.size(); i++)
            {
                model[i]->move_down(dy);
                model[i]->ddy -= dy;
            }
        }

        if (e->nativeVirtualKey() == Qt::Key_D)
        {
            for (unsigned int i = 0; i < model.size(); i++)
            {
                model[i]->move_right(dx);
                model[i]->ddx += dx;
            }
        }

        this->refresh();

        if (flag_c)
        {
            this->draw_web();
        }
        else
        {
            if (ui->check_textures->isChecked())
                this->draw_textures();
            else
                this->draw_tonned();
        }

    }
}

void MainWindow::ctrl_plus()
{
    if (!model_name.empty())
    {
        float dkx = 1.2;
        float dtkx = 1.2;

        kx *= dkx;
        ky *= dkx;
        tkx /= dtkx;
        tky /= dtkx;

        this->refresh();

        if (flag_c)
        {
            this->draw_web();
        }
        else
        {
            if (ui->check_textures->isChecked())
                this->draw_textures();
            else
                this->draw_tonned();
        }

    }
}

void MainWindow::ctrl_minus()
{
    if (!model_name.empty())
    {
        float dkx = 1.2;
        float dtkx = 1.2;

        kx /= dkx;
        ky /= dkx;
        tkx *= dtkx;
        tky *= dtkx;

        this->refresh();

        if (flag_c)
        {
            this->draw_web();
        }
        else
        {
            if (ui->check_textures->isChecked())
                this->draw_textures();
            else
                this->draw_tonned();
        }

    }
}

void MainWindow::shift_w()
{
    if (!model_name.empty())
    {
        eye = Vec3f(eye[0], eye[1] + 1, eye[2]);

        this->refresh();

        if (ui->check_textures->isChecked() and flag_t)
        {
            this->draw_textures();
        }
    }
}

void MainWindow::shift_s()
{
    if (!model_name.empty())
    {
        eye = Vec3f(eye[0], eye[1] - 1, eye[2]);

        this->refresh();

        if (ui->check_textures->isChecked() and flag_t)
        {
            this->draw_textures();
        }
    }
}

void MainWindow::shift_a()
{
    if (!model_name.empty())
    {
        eye = Vec3f(eye[0] - 1, eye[1], eye[2]);

        this->refresh();

        if (ui->check_textures->isChecked() and flag_t)
        {
            this->draw_textures();
        }
    }
}

void MainWindow::shift_d()
{
    if (!model_name.empty())
    {
        eye = Vec3f(eye[0] + 1, eye[1], eye[2]);

        this->refresh();

        if (ui->check_textures->isChecked() and flag_t)
        {
            this->draw_textures();
        }
    }
}
