#include "my_gl.h"

void IShader::set_model(Model *m)
{
    this->model = m;
}

Vec4f Shader::vertex(int iface, int nthvert)
{
    varying_uv.set_col(nthvert, model->uv(iface, nthvert));
    varying_nrm.set_col(nthvert, proj<3>((Projection*ModelView).invert_transpose()*embed<4>(model->normal(iface, nthvert), 0.f)));
    Vec4f gl_Vertex = Projection*ModelView*embed<4>(model->vert(iface, nthvert));
    varying_tri.set_col(nthvert, gl_Vertex);
    ndc_tri.set_col(nthvert, proj<3>(gl_Vertex/gl_Vertex[3]));
    return gl_Vertex;
}

bool Shader::fragment(Vec3f bar, TGAColor &color)
{
    Vec3f bn = (varying_nrm*bar).normalize();
    Vec2f uv = varying_uv*bar;

    mat<3,3,float> A;
    A[0] = ndc_tri.col(1) - ndc_tri.col(0);
    A[1] = ndc_tri.col(2) - ndc_tri.col(0);
    A[2] = bn;

    mat<3,3,float> AI = A.invert();

    Vec3f i = AI * Vec3f(varying_uv[0][1] - varying_uv[0][0], varying_uv[0][2] - varying_uv[0][0], 0);
    Vec3f j = AI * Vec3f(varying_uv[1][1] - varying_uv[1][0], varying_uv[1][2] - varying_uv[1][0], 0);

    mat<3,3,float> B;
    B.set_col(0, i.normalize());
    B.set_col(1, j.normalize());
    B.set_col(2, bn);

    Vec3f n = (B*model->normal(uv)).normalize();

    float diff = std::max(0.f, n*light_dir2);
    color = model->diffuse(uv)*diff;

    return false;
}

IShader::~IShader()
{

}

void viewport(Matrix &V, int x, int y, int w, int h)
{
    V = Matrix::identity();
    V[0][3] = x+w/2.f;
    V[1][3] = y+h/2.f;
    V[2][3] = 1.f;
    V[0][0] = w/2.f;
    V[1][1] = h/2.f;
    V[2][2] = 0;
}

void projection(Matrix &P, float coeff)
{
    P = Matrix::identity();
    P[3][2] = coeff;
}

void lookat(Matrix &MV, Vec3f eye, Vec3f center, Vec3f up)
{
    Vec3f z = (eye-center).normalize();
    Vec3f x = cross(up,z).normalize();
    Vec3f y = cross(z,x).normalize();
    Matrix Minv = Matrix::identity();
    Matrix Tr = Matrix::identity();
    for (unsigned int i = 0; i < 3; i++)
    {
        Minv[0][i] = x[i];
        Minv[1][i] = y[i];
        Minv[2][i] = z[i];
        Tr[i][3] = -center[i];
    }

    MV = Minv*Tr;
}

void line(int x0, int y0, int x1, int y1, TGAImage &image, TGAColor color)
{
    if (x0==x1 && y0==y1)
    {
        image.set(x1, y1, color);
        return;
    }

    const int dx = std::abs(x1 - x0);
    const int dy = std::abs(y1 - y0);
    const int sign_x = x0<x1 ? 1 : -1;
    const int sign_y = y0<y1 ? 1 : -1;

    int e = dx-dy;

    image.set(x1, y1, color);
    while (x0!=x1 || y0!=y1)
    {
        image.set(x0, y0, color);
        const int e2 = 2*e;

        if (e2>-dy)
        {
            e -= dy;
            x0 += sign_x;
        }

        if (e2<dx)
        {
            e += dx;
            y0 += sign_y;
        }
    }
}

void line(int x0, int y0, int x1, int y1, QPainter *image, QColor color)
{
    image->setPen(color);
    if (x0==x1 && y0==y1)
    {
        image->drawPoint(x1, y1);
        return;
    }

    const int dx = std::abs(x1 - x0);
    const int dy = std::abs(y1 - y0);
    const int sign_x = x0<x1 ? 1 : -1;
    const int sign_y = y0<y1 ? 1 : -1;

    int e = dx-dy;

    image->drawPoint(x1, y1);
    while (x0!=x1 || y0!=y1)
    {
        image->drawPoint(x0, y0);
        const int e2 = 2*e;

        if (e2>-dy)
        {
            e -= dy;
            x0 += sign_x;
        }

        if (e2<dx)
        {
            e += dx;
            y0 += sign_y;
        }
    }
}

void triangle(Vec3i t0, Vec3i t1, Vec3i t2, TGAImage &image, TGAColor color, int *zbuffer)
{
    if (t0.y==t1.y && t0.y==t2.y)
        return;
    if (t0.y>t1.y)
        std::swap(t0, t1);
    if (t0.y>t2.y)
        std::swap(t0, t2);
    if (t1.y>t2.y)
        std::swap(t1, t2);

    int total_height = t2.y-t0.y;
    for (int i=0; i<total_height; i++)
    {
        bool second_half = i>t1.y-t0.y || t1.y==t0.y;
        int segment_height = second_half ? t2.y-t1.y : t1.y-t0.y;
        float alpha = (float)i/total_height;
        float beta  = (float)(i-(second_half ? t1.y-t0.y : 0))/segment_height;
        Vec3i A =               t0 + Vec3f(t2-t0)*alpha;
        Vec3i B = second_half ? t1 + Vec3f(t2-t1)*beta : t0 + Vec3f(t1-t0)*beta;
        if (A.x>B.x) std::swap(A, B);
        for (int j=A.x; j<=B.x; j++)
        {
            float phi = B.x==A.x ? 1. : (float)(j-A.x)/(float)(B.x-A.x);
            Vec3i P = Vec3f(A) + Vec3f(B-A)*phi;
            int idx = P.x+P.y*800;
            if (zbuffer[idx]<P.z)
            {
                zbuffer[idx] = P.z;
                image.set(P.x, P.y, color);
            }
        }
    }
}

void triangle(Vec3i t0, Vec3i t1, Vec3i t2, QPainter *image, QColor color, int *zbuffer)
{
    if (t0.y==t1.y && t0.y==t2.y)
        return;
    if (t0.y>t1.y)
        std::swap(t0, t1);
    if (t0.y>t2.y)
        std::swap(t0, t2);
    if (t1.y>t2.y)
        std::swap(t1, t2);

    int total_height = t2.y-t0.y;
    image->setPen(color);

    for (int i = 0; i < total_height; i++)
    {
        bool second_half = i > t1.y-t0.y || t1.y == t0.y;
        int segment_height = second_half ? t2.y-t1.y : t1.y-t0.y;
        float alpha = (float)i/total_height;
        float beta  = (float)(i-(second_half ? t1.y-t0.y : 0))/segment_height;
        Vec3i A =               t0+Vec3f(t2-t0)*alpha;
        Vec3i B = second_half ? t1+Vec3f(t2-t1)*beta : t0+Vec3f(t1-t0)*beta;
        if (A.x > B.x)
            std::swap(A, B);
        for (int j = A.x; j <= B.x; j++)
        {
            float phi = B.x==A.x ? 1. : (float)(j-A.x)/(float)(B.x-A.x);
            Vec3i P = Vec3f(A) + Vec3f(B-A)*phi;
            int idx = P.x+P.y*800;
            if (idx < 800*800 && idx >= 0 && zbuffer[idx]<P.z)
            {
                zbuffer[idx] = P.z;
                image->drawPoint(P.x, P.y);
            }
        }
    }
}

Vec3f barycentric(Vec2f A, Vec2f B, Vec2f C, Vec2f P)
{
    Vec3f s[2];
    for (int i=2; i--; )
    {
        s[i][0] = C[i]-A[i];
        s[i][1] = B[i]-A[i];
        s[i][2] = A[i]-P[i];
    }
    Vec3f u = cross(s[0], s[1]);
    if (std::abs(u[2])>1e-2)
        return Vec3f(1.f-(u.x+u.y)/u.z, u.y/u.z, u.x/u.z);
    return Vec3f(-1,1,1);
}

void triangle(mat<4,3,float> &clipc, IShader &shader, TGAImage &image, float *zbuffer)
{
    mat<3,4,float> pts  = (Viewport*clipc).transpose();
    mat<3,2,float> pts2;
    for (int i=0; i<3; i++) pts2[i] = proj<2>(pts[i]/pts[i][3]);

    Vec2f bboxmin( std::numeric_limits<float>::max(),  std::numeric_limits<float>::max());
    Vec2f bboxmax(-std::numeric_limits<float>::max(), -std::numeric_limits<float>::max());
    Vec2f clamp(image.get_width()-1, image.get_height()-1);
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<2; j++)
        {
            bboxmin[j] = std::max(0.f,      std::min(bboxmin[j], pts2[i][j]));
            bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts2[i][j]));
        }
    }
    Vec2i P;
    TGAColor color;
    for (P.x=bboxmin.x; P.x<=bboxmax.x; P.x++)
    {
        for (P.y=bboxmin.y; P.y<=bboxmax.y; P.y++)
        {
            Vec3f bc_screen  = barycentric(pts2[0], pts2[1], pts2[2], P);
            Vec3f bc_clip    = Vec3f(bc_screen.x/pts[0][3], bc_screen.y/pts[1][3], bc_screen.z/pts[2][3]);
            bc_clip = bc_clip/(bc_clip.x+bc_clip.y+bc_clip.z);
            float frag_depth = clipc[2]*bc_clip;
            if (bc_screen.x<0 || bc_screen.y<0 || bc_screen.z<0 || zbuffer[P.x+P.y*image.get_width()]>frag_depth) continue;
            bool discard = shader.fragment(bc_clip, color);
            if (!discard)
            {
                zbuffer[P.x+P.y*image.get_width()] = frag_depth;
                image.set(P.x, P.y, color);
            }
        }
    }
}

void triangle(mat<4,3,float> &clipc, IShader &shader, QPainter *image, float *zbuffer)
{
    mat<3,4,float> pts  = (Viewport*clipc).transpose();
    mat<3,2,float> pts2;
    for (unsigned int i = 0; i < 3; i++)
        pts2[i] = proj<2>(pts[i]/pts[i][3]);

    Vec2f bboxmin( std::numeric_limits<float>::max(),  std::numeric_limits<float>::max());
    Vec2f bboxmax(-std::numeric_limits<float>::max(), -std::numeric_limits<float>::max());
    Vec2f clamp(599, 679);
    for (unsigned int i = 0; i < 3; i++)
    {
        for (unsigned int j = 0; j < 2; j++)
        {
            bboxmin[j] = std::max(0.f,      std::min(bboxmin[j], pts2[i][j]));
            bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts2[i][j]));
        }
    }
    Vec2i P;
    TGAColor color;
    for (P.x = bboxmin.x; P.x <= bboxmax.x; P.x++)
    {
        for (P.y = bboxmin.y; P.y <= bboxmax.y; P.y++)
        {
            Vec3f bc_screen  = barycentric(pts2[0], pts2[1], pts2[2], P);
            Vec3f bc_clip    = Vec3f(bc_screen.x/pts[0][3], bc_screen.y/pts[1][3], bc_screen.z/pts[2][3]);
            bc_clip = bc_clip/(bc_clip.x+bc_clip.y+bc_clip.z);
            float frag_depth = clipc[2]*bc_clip;
            if (bc_screen.x<0 || bc_screen.y<0 || bc_screen.z<0 || zbuffer[P.x+P.y*680]>frag_depth)
                continue;

            bool discard = shader.fragment(bc_clip, color);
            image->setPen(QColor(color[2], color[1], color[0]));
            if (!discard)
            {
                zbuffer[P.x+P.y*680] = frag_depth;
                image->drawPoint(P.x, 680 - P.y);
            }
        }
    }
}
