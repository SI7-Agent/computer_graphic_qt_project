#include "model.h"

Model::Model(const char *filename):
    verts_(),
    faces_(),
    norms_(),
    uv_(),
    diffusemap_(),
    normalmap_(),
    specularmap_(),
    flag_textures(false),
    ddx(0),
    ddy(0)
{
    std::ifstream in;
    in.open (filename, std::ifstream::in);
    if (in.fail())
        return;

    std::string line;
    while (!in.eof())
    {
        std::getline(in, line);
        std::istringstream iss(line.c_str());
        char trash;
        if (!line.compare(0, 2, "v "))
        {
            iss >> trash;
            Vec3f v;
            for (unsigned int i = 0; i < 3; i++) iss >> v[i];
            verts_.push_back(v);
        }
        else if (!line.compare(0, 3, "vn "))
        {
            iss >> trash >> trash;
            Vec3f n;
            for (unsigned int i = 0; i < 3; i++) iss >> n[i];
            norms_.push_back(n);
        }
        else if (!line.compare(0, 3, "vt "))
        {
            iss >> trash >> trash;
            Vec2f uv;
            for (unsigned int i = 0; i < 2; i++) iss >> uv[i];
            uv_.push_back(uv);
        }
        else if (!line.compare(0, 2, "f "))
        {
            std::vector<Vec3i> f;
            Vec3i tmp;
            iss >> trash;
            while (iss >> tmp[0] >> trash >> tmp[1] >> trash >> tmp[2])
            {
                for (unsigned int i = 0; i < 3; i++) tmp[i]--;
                f.push_back(tmp);
            }
            faces_.push_back(f);
        }
    }
    std::cerr << "# v# " << verts_.size() << " f# "  << faces_.size() << " vt# " << uv_.size() << " vn# " << norms_.size() << std::endl;
    load_texture(filename, "_diffuse.tga", diffusemap_);
    load_texture(filename, "_nm_tangent.tga", normalmap_);
    load_texture(filename, "_spec.tga", specularmap_);
}

Model::~Model()
{

}

int Model::nverts()
{
    return (int)verts_.size();
}

int Model::nfaces()
{
    return (int)faces_.size();
}

std::vector<int> Model::face(int idx)
{
    std::vector<int> face;
    for (int i=0; i<(int)faces_[idx].size(); i++)
        face.push_back(faces_[idx][i][0]);
    return face;
}

void Model::set_vert(int i, Vec3f value)
{
    this->verts_[i] = value;
}

Vec3f Model::vert(int i)
{
    return verts_[i];
}

Vec3f Model::vert(int iface, int nthvert)
{
    return verts_[faces_[iface][nthvert][0]];
}

void Model::load_texture(std::string filename, const char *suffix, TGAImage &img)
{
    std::string texfile(filename);
    size_t dot = texfile.find_last_of(".");
    if (dot != std::string::npos)
    {
        texfile = texfile.substr(0,dot) + std::string(suffix);
        img.read_tga_file(texfile.c_str()) ? flag_textures = true : flag_textures = false;
        img.flip_vertically();
    }
}

TGAColor Model::diffuse(Vec2f uvf)
{
    Vec2i uv(uvf[0]*diffusemap_.get_width(), uvf[1]*diffusemap_.get_height());
    return diffusemap_.get(uv[0], uv[1]);
}

Vec3f Model::normal(Vec2f uvf)
{
    Vec2i uv(uvf[0]*normalmap_.get_width(), uvf[1]*normalmap_.get_height());
    TGAColor c = normalmap_.get(uv[0], uv[1]);
    Vec3f res;
    for (int i=0; i<3; i++)
        res[2-i] = (float)c[i]/255.f*2.f - 1.f;
    return res;
}

Vec2f Model::uv(int iface, int nthvert)
{
    return uv_[faces_[iface][nthvert][1]];
}

float Model::specular(Vec2f uvf)
{
    Vec2i uv(uvf[0]*specularmap_.get_width(), uvf[1]*specularmap_.get_height());
    return specularmap_.get(uv[0], uv[1])[0]/1.f;
}

Vec3f Model::normal(int iface, int nthvert)
{
    int idx = faces_[iface][nthvert][2];
    return norms_[idx].normalize();
}

void Model::rotate_left(double angle)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    angle *= -1;
    auto rotate_thread = [](Model *model, int num, int end, double angle)
    {
        float cos_value = cos(angle);
        float sin_value = sin(angle);

        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_x = model->ddx + (model->vert(i)[0] - model->ddx)*cos_value - (model->vert(i)[2])*sin_value;
            float tmp_z = (model->vert(i)[0] - model->ddx)*sin_value + (model->vert(i)[2])*cos_value;
            Vec3f new_vert(tmp_x, model->vert(i)[1], tmp_z);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num,
                angle);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::rotate_right(double angle)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, double angle)
    {
        float cos_value = cos(angle);
        float sin_value = sin(angle);

        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_x = model->ddx + (model->vert(i)[0] - model->ddx)*cos_value - (model->vert(i)[2])*sin_value;
            float tmp_z = (model->vert(i)[0] - model->ddx)*sin_value + (model->vert(i)[2])*cos_value;
            Vec3f new_vert(tmp_x, model->vert(i)[1], tmp_z);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num,
                angle);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::rotate_down(double angle)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, double angle)
    {
        float cos_value = cos(angle);
        float sin_value = sin(angle);

        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_y = model->ddy + (model->vert(i)[1] - model->ddy)*cos_value - (model->vert(i)[2])*sin_value;
            float tmp_z = (model->vert(i)[1] - model->ddy)*sin_value + (model->vert(i)[2])*cos_value;
            Vec3f new_vert(model->vert(i)[0], tmp_y, tmp_z);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num, angle);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::rotate_up(double angle)
{
//    std::thread threads[THREADS_NUMBER];
//    int verts_num = this->nverts();
//    angle *= -1;
//    auto rotate_thread = [](Model *model, int num, int end, double angle)
//    {
//        float cos_value = cos(angle);
//        float sin_value = sin(angle);

//        for (int i = num; i < end; i += THREADS_NUMBER)
//        {
//            float tmp_y = model->ddy + (model->vert(i)[1] - model->ddy)*cos_value - (model->vert(i)[2])*sin_value;
//            float tmp_z = (model->vert(i)[1] - model->ddy)*sin_value + (model->vert(i)[2])*cos_value;
//            Vec3f new_vert(model->vert(i)[0], tmp_y, tmp_z);
//            model->set_vert(i, new_vert);
//        }
//    };

//    for (int i = 0; i < THREADS_NUMBER; ++i)
//        threads[i] = std::thread(rotate_thread, this, i, verts_num, angle);

//    for (int i = 0; i < THREADS_NUMBER; ++i)
//        if (threads[i].joinable())
//            threads[i].join();

    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    angle *= -1;
    auto rotate_thread = [](Model *model, int num, int end, double angle)
    {
        float cos_value = cos(angle);
        float sin_value = sin(angle);

        for (int i = num; i < end; i += THREADS_NUMBER)
        {
            float tmp_y = model->ddy + (model->vert(i)[1] - model->ddy)*cos_value - (model->vert(i)[2])*sin_value;
            float tmp_z = (model->vert(i)[1] - model->ddy)*sin_value + (model->vert(i)[2])*cos_value;
            Vec3f new_vert(model->vert(i)[0], tmp_y, tmp_z);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num, angle);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::move_left(float dx)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, float dx)
    {
        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_x = model->vert(i)[0] - dx;
            Vec3f new_vert(tmp_x, model->vert(i)[1], model->vert(i)[2]);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num, dx);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::move_right(float dx)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, float dx)
    {
        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_x = model->vert(i)[0] + dx;
            Vec3f new_vert(tmp_x, model->vert(i)[1], model->vert(i)[2]);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num, dx);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::move_down(float dy)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, float dy)
    {
        for (int i = num; i<end; i += THREADS_NUMBER)
        {
            float tmp_y = model->vert(i)[1] - dy;
            Vec3f new_vert(model->vert(i)[0], tmp_y, model->vert(i)[2]);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
       threads[i] = std::thread(rotate_thread, this, i, verts_num, dy);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

void Model::move_up(float dy)
{
    std::thread threads[THREADS_NUMBER];
    int verts_num = this->nverts();
    auto rotate_thread = [](Model *model, int num, int end, float dy)
    {
        for (int i = num; i < end; i += THREADS_NUMBER)
        {
            float tmp_y = model->vert(i)[1] + dy;
            Vec3f new_vert(model->vert(i)[0], tmp_y, model->vert(i)[2]);
            model->set_vert(i, new_vert);
        }
    };

    for (int i = 0; i < THREADS_NUMBER; ++i)
        threads[i] = std::thread(rotate_thread, this, i, verts_num, dy);

    for (int i = 0; i < THREADS_NUMBER; ++i)
        if (threads[i].joinable())
            threads[i].join();
}

TGAImage Model::diffuse_m()
{
    return this->diffusemap_;
}

TGAImage Model::normal_m()
{
    return this->normalmap_;
}

TGAImage Model::specular_m()
{
    return this->specularmap_;
}
